MODULE FLASC
  IMPLICIT NONE

  !!! Global Variables

  DOUBLE PRECISION, PARAMETER :: pi = 4.0*ATAN(1.0)
  DOUBLE PRECISION, PARAMETER :: inf = HUGE(real(inf))
  DOUBLE PRECISION, PARAMETER :: e = EXP(1.0)

  !! Data Type Matrix
  TYPE, PUBLIC :: Matrix
     DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: Entries
     INTEGER :: NoOfRows, NoOfColumns
     LOGICAL :: IsSquare, IsDiagonal, IsScalar, IsDiagonalizable, IsSymmetric, IsSkewSymmetric
   CONTAINS
     PROCEDURE, PASS(Matrix1) :: AddMatrices
     PROCEDURE, PASS(Matrix1) :: SubtractMatrices
     PROCEDURE, PASS(Matrix1) :: MatrixProduct
     PROCEDURE, PASS(Matrix0) :: ScalarLeftProduct
     PROCEDURE, PASS(Matrix0) :: ScalarRightProduct
 
     GENERIC, PUBLIC :: operator(+) => AddMatrices
     GENERIC, PUBLIC :: operator(-) => SubtractMatrices
     GENERIC, PUBLIC :: operator(*) => MatrixProduct, ScalarLeftProduct, ScalarRightProduct
     
     PROCEDURE :: ReadMatrix
     PROCEDURE :: WriteMatrix
     PROCEDURE :: PrintMatrix
     PROCEDURE :: TransposeMatrix
     PROCEDURE :: Trace
     PROCEDURE :: Adjoint
     PROCEDURE :: Inverse
     PROCEDURE :: Determinant
     PROCEDURE :: Minor
     PROCEDURE :: CoFactorMatrix
     PROCEDURE :: CoFactor

     FINAL :: DeAllocateMatrix
  END TYPE Matrix

  !! Data Type Vector
  TYPE, PUBLIC :: Vector
     DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: Components
     INTEGER :: Dimension
     LOGICAL :: IsNull, IsUnit
   CONTAINS
     PROCEDURE, PASS(Vector1) :: AddVectors
     PROCEDURE, PASS(Vector1) :: SubtractVectors
     PROCEDURE, PASS(Vector1) :: DotProduct
     PROCEDURE, PASS(Vector1) :: CrossProduct
     PROCEDURE, PASS(Vector0) :: ScalarLeftProductVectors
     PROCEDURE, PASS(Vector0) :: ScalarRightProductVectors

     GENERIC, PUBLIC :: operator(+) => AddVectors
     GENERIC, PUBLIC :: operator(-) => SubtractVectors
     GENERIC, PUBLIC :: operator(*) => ScalarLeftProductVectors, ScalarRightProductVectors
     GENERIC, PUBLIC :: operator(.DOT.) => DotProduct
     GENERIC, PUBLIC :: operator(.CROSS.) => CrossProduct
     
     PROCEDURE :: ReadVector
     PROCEDURE :: WriteVector
     PROCEDURE :: PrintVector

     FINAL :: DeAllocateVector
  END TYPE Vector


  CONTAINS
  
  !! Matrix Functions

  ! Constructor (Optional for Dependent Matrix)
  FUNCTION SetMatrix(Row, Column) RESULT(OutputMatrix) 
       INTEGER :: Row, Column
       TYPE(Matrix) :: OutputMatrix
       OutputMatrix%NoOfRows = Row
       OutputMatrix%NoOfColumns = Column
       ALLOCATE(OutputMatrix%Entries(Row,Column))
       IF (OutputMatrix%NoOfRows .EQ. OutputMatrix%NoOfColumns) THEN
        OutputMatrix%IsSquare = .TRUE.
       ELSE 
        OutputMatrix%IsSquare = .FALSE.
       END IF
  END FUNCTION SetMatrix

  ! Read Matrix From a FILE
  SUBROUTINE ReadMatrix(OutputMatrix, InputFile)
   CHARACTER(len = *) :: InputFile
   CLASS(Matrix) :: OutputMatrix
   INTEGER :: i
   OPEN(unit=999, file=InputFile, status="old")
    DO i=1,OutputMatrix%NoOfRows
       READ(999, *) OutputMatrix%Entries(i, :)
    END DO
   CLOSE(999)
  END SUBROUTINE ReadMatrix

  ! Write Matrix to a FILE
  SUBROUTINE WriteMatrix(InputMatrix, OutputFile, Format)
    CHARACTER(len = *) :: OutputFile
    CLASS(Matrix) :: InputMatrix
    CHARACTER(len = *), OPTIONAL :: Format
    INTEGER :: i
    OPEN(unit=999, file=OutputFile)
     DO i=1,InputMatrix%NoOfRows
      IF(PRESENT(Format)) THEN
        WRITE(999, Format) InputMatrix%Entries(i, :)
      ELSE
        WRITE(999, *) InputMatrix%Entries(i, :)
      END IF
     END DO
    CLOSE(999)
  END SUBROUTINE WriteMatrix

  ! Print Matrix to the Buffer
  SUBROUTINE PrintMatrix(InputMatrix, Format)
    CLASS(Matrix) :: InputMatrix
    INTEGER :: i
    CHARACTER(len = *), OPTIONAL :: Format
    DO i=1,InputMatrix%NoOfRows
      IF(PRESENT(Format)) THEN
        WRITE(*, Format) InputMatrix%Entries(i, :)
      ELSE
        WRITE(*, *) InputMatrix%Entries(i, :)
      END IF
     END DO
  END SUBROUTINE PrintMatrix

  ! Destructor
  SUBROUTINE DeAllocateMatrix(this)
    TYPE(Matrix) :: this
    IF (ALLOCATED(this%Entries)) DEALLOCATE(this%Entries)
  END SUBROUTINE DeAllocateMatrix
  
  ! Add Matrices 
  FUNCTION AddMatrices(Matrix1, Matrix2) RESULT(Sum)
    CLASS(Matrix), INTENT(IN) :: Matrix1, Matrix2
    TYPE(Matrix) :: Sum
    INTEGER :: i, j
    Sum = SetMatrix(Matrix1%NoOfRows,Matrix1%NoOfColumns)
    IF ((Matrix1%NoOfRows .EQ. Matrix2%NoOfRows) .AND. (Matrix1%NoOfColumns .EQ. Matrix2%NoOfColumns)) THEN
      DO i=1,Sum%NoOfRows
        DO j=1,Sum%NoOfColumns
          Sum%Entries(i, j) = Matrix1%Entries(i, j) + Matrix2%Entries(i,j)
        END DO
      END DO
    ELSE
      WRITE(*, *) "Error in Addition, Matrix Order Mismatch."
    END IF
  END FUNCTION AddMatrices
  
  ! Subtract Matrices
  FUNCTION SubtractMatrices(Matrix1, Matrix2) RESULT(Difference)
    CLASS(Matrix), INTENT(IN) :: Matrix1, Matrix2
    TYPE(Matrix) :: Difference
    INTEGER :: i, j
    Difference = SetMatrix(Matrix1%NoOfRows,Matrix1%NoOfColumns)
    IF ((Matrix1%NoOfRows .EQ. Matrix2%NoOfRows) .AND. (Matrix1%NoOfColumns .EQ. Matrix2%NoOfColumns)) THEN
      DO i=1,Difference%NoOfRows
        DO j=1,Difference%NoOfColumns
          Difference%Entries(i, j) = Matrix1%Entries(i, j) - Matrix2%Entries(i,j)
        END DO
      END DO
    ELSE
      WRITE(*, *) "Error in Subtraction, Matrix Order Mismatch."
   END IF
  END FUNCTION SubtractMatrices

  ! Matrix Product
  FUNCTION MatrixProduct(Matrix1, Matrix2) RESULT(MatProduct)
    CLASS(Matrix), INTENT(IN) :: Matrix1, Matrix2
    TYPE(Matrix) :: MatProduct
    INTEGER :: i, j, m
    MatProduct = SetMatrix(Matrix1%NoOfRows,Matrix2%NoOfColumns)
    IF (Matrix1%NoOfColumns .EQ. Matrix2%NoOfRows) THEN
      DO i=1,MatProduct%NoOfRows
        DO j=1,MatProduct%NoOfColumns
          MatProduct%Entries(i, j) = 0
          DO m=1, Matrix2%NoOfRows
            MatProduct%Entries(i, j) = MatProduct%Entries(i, j) + ((Matrix1%Entries(i, m)) * (Matrix2%Entries(m, j)))
          END DO      
        END DO
      END DO
    ELSE
      WRITE(*, *) "Error in Multiplication, Matrix Order Mismatch."
   END IF
  END FUNCTION MatrixProduct

  ! Multiplying a Scalar on Left
  FUNCTION ScalarLeftProduct(ScalingFactor, Matrix0) RESULT(ScaledMatrix)
    CLASS(Matrix), INTENT(IN) :: Matrix0
    DOUBLE PRECISION, INTENT(IN) :: ScalingFactor
    TYPE(Matrix) :: ScaledMatrix
    INTEGER :: i, j
    ScaledMatrix = SetMatrix(Matrix0%NoOfRows,Matrix0%NoOfColumns)
    DO i=1,ScaledMatrix%NoOfRows
      DO j=1,ScaledMatrix%NoOfColumns
        ScaledMatrix%Entries(i,j) = ScalingFactor * (Matrix0%Entries(i,j))
      END DO
    END DO
  END FUNCTION ScalarLeftProduct
  
  ! Multiplying a Scalar on Right
  FUNCTION ScalarRightProduct(Matrix0, ScalingFactor) RESULT(ScaledMatrix)
    CLASS(Matrix), INTENT(IN) :: Matrix0
    TYPE(Matrix) :: ScaledMatrix
    DOUBLE PRECISION, INTENT(IN) :: ScalingFactor
    INTEGER :: i, j
    ScaledMatrix = SetMatrix(Matrix0%NoOfRows,Matrix0%NoOfColumns)
    DO i=1,ScaledMatrix%NoOfRows
      DO j=1,ScaledMatrix%NoOfColumns
        ScaledMatrix%Entries(i, j) = ScalingFactor * (Matrix0%Entries(i,j))
      END DO
    END DO
  END FUNCTION ScalarRightProduct

  ! Transpose of a Matrix
  FUNCTION TransposeMatrix(InputMatrix) RESULT(TransposedMatrix)
    CLASS(Matrix), INTENT(IN) :: InputMatrix
    INTEGER :: i, j
    TYPE(Matrix) :: TransposedMatrix
    TransposedMatrix = SetMatrix(InputMatrix%NoOfColumns,InputMatrix%NoOfRows)
      DO i=1,InputMatrix%NoOfRows
        DO j=1,InputMatrix%NoOfColumns
          TransposedMatrix%Entries(j, i) = InputMatrix%Entries(i, j)
        END DO
      END DO
  END FUNCTION TransposeMatrix

  ! Trace of a Matrix
  FUNCTION Trace(InputMatrix) RESULT(DiagSum)
    CLASS(Matrix), INTENT(IN) :: InputMatrix
    DOUBLE PRECISION :: DiagSum
    INTEGER :: i, j
    IF (InputMatrix%IsSquare .EQV. .TRUE.) THEN
      DO i=1,InputMatrix%NoOfRows
        DO j=1,InputMatrix%NoOfColumns
          IF (i .EQ. j) THEN
            DiagSum = DiagSum + InputMatrix%Entries(i, j)
          END IF
        END DO
      END DO
    ELSE
      WRITE(*, *) "Error in Finding Trace, Matrix is not a Square Matrix."
    END IF
  END FUNCTION Trace

  ! Determinant
  RECURSIVE DOUBLE PRECISION FUNCTION Determinant(InputMatrix) RESULT(OutputDeterminant)
    IMPLICIT NONE
    CLASS(Matrix), INTENT(in) :: InputMatrix
    TYPE(Matrix) :: SubMatrix
    INTEGER :: Order, i
    Order = InputMatrix%NoOfRows
    IF (InputMatrix%IsSquare .EQV. .TRUE.) THEN     
     IF (Order .EQ. 1) THEN
       OutputDeterminant = InputMatrix%Entries(1,1)
     ELSE
       SubMatrix = SetMatrix(Order-1, Order-1)
       OutputDeterminant = 0.0d0
       DO i=1,Order
         SubMatrix%Entries(:,:(i-1)) = InputMatrix%Entries(2:,:i-1)
         SubMatrix%Entries(:,i:) = InputMatrix%Entries(2:,i+1:)
         OutputDeterminant = OutputDeterminant + (((-1)**(DBLE(i+1)))*(InputMatrix%Entries(1,i))*(Determinant(SubMatrix)))
       ENDDO
     ENDIF
    ELSE
      WRITE(*, *) "Error in Finding Determinant, Matrix is not a Square Matrix."
    ENDIF
  END FUNCTION Determinant

  ! Minor of an Entry of Matrix
  FUNCTION Minor(InputMatrix, RowOfEntry, ColumnOfEntry) RESULT(MinorOut)
    CLASS(Matrix), INTENT(IN) :: InputMatrix
    TYPE(Matrix) :: SubMatrix
    DOUBLE PRECISION :: MinorOut
    INTEGER :: Order, i, j, RowOfEntry, ColumnOfEntry
    IF (InputMatrix%IsSquare .EQV. .TRUE.) THEN   
     Order = InputMatrix%NoOfRows
     SubMatrix = SetMatrix(Order-1,Order-1)
     IF (Order .EQ. 1) THEN
         MinorOut = InputMatrix%Entries(1,1)
     ELSE
         i = RowOfEntry
         j = ColumnOfEntry
         SubMatrix%Entries(:i-1,:j-1) = InputMatrix%Entries(:i-1,:j-1)
         SubMatrix%Entries(:i-1,j:) = InputMatrix%Entries(:i-1,j+1:)
         SubMatrix%Entries(i:,:j-1) = InputMatrix%Entries(i+1:,:j-1)
         SubMatrix%Entries(i:,j:) = InputMatrix%Entries(i+1:,j+1:)
         MinorOut = Determinant(SubMatrix)
     ENDIF
    ENDIF
  END FUNCTION Minor

  ! Cofactor of an Entry of Matrix
  FUNCTION CoFactor(InputMatrix, RowOfEntry, ColumnOfEntry) RESULT(CoFactorOut)
    CLASS(Matrix), INTENT(IN) :: InputMatrix
    DOUBLE PRECISION :: CoFactorOut
    INTEGER :: RowOfEntry, ColumnOfEntry
    CoFactorOut = ((-1.0d0)**DBLE(RowOfEntry + ColumnOfEntry)) * Minor(InputMatrix, RowOfEntry, ColumnOfEntry)
  END FUNCTION CoFactor

  ! Cofactor Matrix
  FUNCTION CoFactorMatrix(InputMatrix) RESULT(CoFactorMatrixOut)
    CLASS(Matrix), INTENT(IN) :: InputMatrix
    TYPE(Matrix) :: CoFactorMatrixOut
    INTEGER :: Order, i, j
    Order = InputMatrix%NoOfColumns
    IF (InputMatrix%IsSquare .EQV. .TRUE.) THEN   
     CoFactorMatrixOut = SetMatrix(InputMatrix%NoOfRows,InputMatrix%NoOfColumns)
       DO i=1,Order
        DO j=1,Order
         CoFactorMatrixOut%Entries(i,j) = CoFactor(InputMatrix,i,j)
        ENDDO
      END DO
    ENDIF
  END FUNCTION CoFactorMatrix

  ! Adjoint of a Matrix
  FUNCTION Adjoint(InputMatrix) RESULT(AdjointMatrix)
    CLASS(Matrix), INTENT(IN) :: InputMatrix
    TYPE(Matrix) :: AdjointMatrix
    AdjointMatrix = SetMatrix(InputMatrix%NoOfRows,InputMatrix%NoOfColumns)
    AdjointMatrix = TransposeMatrix(CoFactorMatrix(InputMatrix))
  END FUNCTION Adjoint

  ! Inverse of a Matrix (Using the defenition of Adjoint)
  FUNCTION Inverse(InputMatrix) RESULT(InverseMatrix)
    CLASS(Matrix), INTENT(IN) :: InputMatrix
    TYPE(Matrix) :: InverseMatrix
    IF(Determinant(InputMatrix) .NE. real(0)) THEN
        InverseMatrix = SetMatrix(InputMatrix%NoOfRows,InputMatrix%NoOfColumns)
        InverseMatrix = ((DBLE(1.0))/Determinant(InputMatrix))*(Adjoint(InputMatrix))
    ELSE
      WRITE(*, *) "Error in Finding Inverse, Matrix is Singular."
    END IF
  END FUNCTION Inverse

  !! Vector Functions

  ! Constructor (Optional for Dependent Matrix)
  FUNCTION SetVector(Dimension) RESULT(OutputVector) 
    INTEGER :: Dimension
    TYPE(Vector) :: OutputVector
    OutputVector%Dimension = Dimension
    ALLOCATE(OutputVector%Components(Dimension))
  END FUNCTION SetVector

  ! Read Vector From a FILE
  SUBROUTINE ReadVector(OutputVector, InputFile)
    CHARACTER(len = *) :: InputFile
    CLASS(Vector) :: OutputVector
    OPEN(unit=999, file=InputFile, status="old")
      READ(999, *) OutputVector%Components
    CLOSE(999)
  END SUBROUTINE ReadVector

  ! Write Vector to a FILE
  SUBROUTINE WriteVector(InputVector, OutputFile, Format)
    CHARACTER(len = *) :: OutputFile
    CLASS(Vector) :: InputVector
    CHARACTER(len = *), OPTIONAL :: Format
    OPEN(unit=999, file=OutputFile)
       IF(PRESENT(Format)) THEN
         WRITE(999, Format) InputVector%Components
       ELSE
         WRITE(999, *) InputVector%Components
       END IF
    CLOSE(999)
  END SUBROUTINE WriteVector

  ! Print Vector to the Buffer
  SUBROUTINE PrintVector(InputVector, Format)
    CLASS(Vector) :: InputVector
    CHARACTER(len = *), OPTIONAL :: Format
      IF(PRESENT(Format)) THEN
        WRITE(*, Format) InputVector%Components
      ELSE
        WRITE(*, *) InputVector%Components
      END IF
  END SUBROUTINE PrintVector

  ! Destructor
  SUBROUTINE DeAllocateVector(this)
    TYPE(Vector) :: this
    IF (ALLOCATED(this%Components)) DEALLOCATE(this%Components)
  END SUBROUTINE DeAllocateVector

  ! Add Vectors 
  FUNCTION AddVectors(Vector1, Vector2) RESULT(Sum)
    CLASS(Vector), INTENT(IN) :: Vector1, Vector2
    TYPE(Vector) :: Sum
    INTEGER :: i
    IF (Vector1%Dimension .EQ. Vector2%Dimension) THEN
      Sum = SetVector(Vector1%Dimension)
      DO i=1,Vector1%Dimension
        Sum%Components(i) = Vector1%Components(i) + Vector2%Components(i)
      END DO
    ELSE
    WRITE(*, *) "Error in Addition, Vector Dimension Mismatch."
    END IF
  END FUNCTION AddVectors

  ! Subtract Matrices
  FUNCTION SubtractVectors(Vector1, Vector2) RESULT(Difference)
    CLASS(Vector), INTENT(IN) :: Vector1, Vector2
    TYPE(Vector) :: Difference
    INTEGER :: i
    IF (Vector1%Dimension .EQ. Vector2%Dimension) THEN
        Difference = SetVector(Vector1%Dimension)
         DO i=1,Vector1%Dimension
            Difference%Components(i) = Vector1%Components(i) - Vector2%Components(i)
        END DO
    ELSE
     WRITE(*, *) "Error in Subtraction, Vector Dimension Mismatch."
    END IF
  END FUNCTION SubtractVectors

  ! Multiplying a Scalar on Left
  FUNCTION ScalarLeftProductVectors(ScalingFactor, Vector0) RESULT(ScaledVector)
    CLASS(Vector), INTENT(IN) :: Vector0
    DOUBLE PRECISION, INTENT(IN) :: ScalingFactor
    TYPE(Vector) :: ScaledVector
    INTEGER :: i
    ScaledVector = SetVector(Vector0%Dimension)
    DO i=1,Vector0%Dimension
      ScaledVector%Components(i) = ScalingFactor * Vector0%Components(i)
    END DO 
  END FUNCTION ScalarLeftProductVectors

  ! Multiplying a Scalar on Right
  FUNCTION ScalarRightProductVectors(Vector0, ScalingFactor) RESULT(ScaledVector)
    CLASS(Vector), INTENT(IN) :: Vector0
    DOUBLE PRECISION, INTENT(IN) :: ScalingFactor
    TYPE(Vector) :: ScaledVector
    INTEGER :: i
    ScaledVector = SetVector(Vector0%Dimension)
    DO i=1,Vector0%Dimension
      ScaledVector%Components(i) = ScalingFactor * Vector0%Components(i)
    END DO 
  END FUNCTION ScalarRightProductVectors

  ! Dot Product
  FUNCTION DotProduct(Vector1, Vector2) RESULT(Dot)
    CLASS(Vector), INTENT(IN) :: Vector1, Vector2
    DOUBLE PRECISION :: Dot
    INTEGER :: i
    IF(Vector1%Dimension .EQ. Vector2%Dimension) THEN
      Dot = 0.0
      DO i=1,Vector1%Dimension
        Dot = Dot + Vector1%Components(i) * Vector2%Components(i)
      END DO 
    ELSE
      WRITE(*, *) "Error in Dot Product, Vector Dimension Mismatch."
    END IF
  END FUNCTION DotProduct

  ! Cross Product (Only defined for 3D Vectors)
  FUNCTION CrossProduct(Vector1, Vector2) RESULT(Cross)
    CLASS(Vector), INTENT(IN) :: Vector1, Vector2
    TYPE(Vector) :: Cross
    IF((Vector1%Dimension .EQ. 3) .EQV. (Vector2%Dimension .EQ. 3)) THEN
      Cross = SetVector(3)
      Cross%Components(1) = Vector1%Components(2) * Vector2%Components(3) - Vector1%Components(3) * Vector2%Components(2)
      Cross%Components(2) = Vector1%Components(3) * Vector2%Components(1) - Vector1%Components(1) * Vector2%Components(3)
      Cross%Components(3) = Vector1%Components(1) * Vector2%Components(2) - Vector1%Components(2) * Vector2%Components(1)
    ELSE IF(Vector1%Dimension .EQ. Vector2%Dimension) THEN
      WRITE(*, *) "Only 3D Vectors are Supported"
    ELSE
      WRITE(*, *) "Error in Cross Product, Vector Dimension Mismatch."
    END IF
  END FUNCTION CrossProduct




  FUNCTION ContinuousSimpsonOneThirdIntegration(InputFunction, LowerBound, UpperBound, n) RESULT(Integral)
    DOUBLE PRECISION :: Integral, h, LowerBound, UpperBound, S, InputFunction
    INTEGER :: n, i
    IF(MOD(n,2) .EQ. 1) n = n+1
    S = 0.0
    h = (UpperBound - LowerBound)/DBLE(n)
    DO i = 2,(n-2),2
       S = S + 2.0*InputFunction(LowerBound + i*h) + 4.0*InputFunction(LowerBound + (i+1)*h)
    END DO
    Integral = (h/real(3))*(S + InputFunction(LowerBound) + InputFunction(UpperBound) + 4.0*InputFunction(LowerBound+h))
  END FUNCTION ContinuousSimpsonOneThirdIntegration

  FUNCTION ContinuousTrapezoidalIntegration(InputFunction, LowerBound, UpperBound, n) RESULT(Integral)
    DOUBLE PRECISION :: Integral, h, LowerBound, UpperBound, S, InputFunction
    INTEGER :: n, i
    S = 0.0
    h = (UpperBound - LowerBound)/DBLE(n)
    DO i = 1,(n-1)
       S = S + 2.0*InputFunction(LowerBound + i*h)
    END DO
    Integral = (h/real(2))*(S + InputFunction(LowerBound) + InputFunction(UpperBound))
  END FUNCTION ContinuousTrapezoidalIntegration

  FUNCTION ContinuousMidpointIntegration(InputFunction, LowerBound, UpperBound, n) RESULT(Integral)
    DOUBLE PRECISION :: Integral, h, LowerBound, UpperBound, S, InputFunction
    INTEGER :: n, i
    S = 0.0
    h = (UpperBound - LowerBound)/DBLE(n)
    DO i = 0,(n-1)
       S = S + InputFunction(((LowerBound + DBLE(i)*h)+(LowerBound + DBLE(i+1)*h))/2)
    END DO
    Integral = h*S
  END FUNCTION ContinuousMidpointIntegration

END MODULE FLASC